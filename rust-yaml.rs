#!/usr/bin/env rust-script

//! ```cargo
//! [dependencies]
//! yaml-rust = "0.4.5"
//! ```

extern crate yaml_rust;

use std::env;
use std::fs;
use yaml_rust::YamlLoader;

pub fn main() {
    for fil in env::args().skip(1) {
        let raw = fs::read_to_string(&fil).unwrap();
        let docs = YamlLoader::load_from_str(&raw).unwrap();
        let data = &docs.to_vec()[0];

        println!("working on {}", &fil);
        println!(
            "first value? {}",
            data[0].as_str().unwrap_or("data is not a string")
        );
        println!(
            "second value? {}",
            data[1].as_f64().unwrap()
        );
        println!();
    }
}
