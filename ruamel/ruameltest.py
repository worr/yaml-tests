#!/usr/bin/env python

from pathlib import Path
from typing import List
import sys

import ruamel.yaml

def main(files: List[str]) -> int:
    y = ruamel.yaml.YAML()
    for fil in files[1:]:
        print(f"working on {fil}")
        data = y.load(Path(fil))

        print(f"data[0] is {data[0]} with type {type(data[0])}")
        print(f"data[1] is {data[1]} with type {type(data[1])}")
        print()

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
