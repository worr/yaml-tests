#!/usr/bin/env node

import fs from 'fs';
import process from 'process';
import YAML from 'yaml';

const args = process.argv.slice(2);

for (const arg of args) {
	const file = fs.readFileSync(arg, 'utf8');
	console.log(`operating on ${arg}`);

	const results = YAML.parse(file);
	console.log(`first value: ${typeof results[0]}: ${results[0]}`);
	console.log(`second value: ${typeof results[1]}: ${results[1]}`);
	console.log();
}
