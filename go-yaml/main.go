package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"reflect"

	"gopkg.in/yaml.v3"
)

func main() {
	fils := os.Args[1:]
	for _, fil := range fils {
		fmt.Printf("looking at %v\n", fil)

		f, err := os.Open(fil)
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not open %v: %v\n", fil, err)
			fmt.Println()
			continue
		}

		raw, err := ioutil.ReadAll(f)
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not read data %v: %v\n", fil, err)
			fmt.Println()
			continue
		}

		data := []interface{}{}
		err = yaml.Unmarshal(raw, &data)
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not parse %v: %v\n", fil, err)
			fmt.Println()
			continue
		}

		fmt.Printf("type of first %v\n", reflect.TypeOf(data[0]))
		fmt.Printf("type of second %v\n", reflect.TypeOf(data[1]))
		fmt.Println()
	}
}
