module gitlab.com/worr/yaml/go-yaml

go 1.18

require gopkg.in/yaml.v3 v3.0.0-20220512140231-539c8e751b99 // indirect
